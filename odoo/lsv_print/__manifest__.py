{
    'name': "LSV Print module",
    'author': "LSV-Tech",
    'version': "1.0.0-dev26",
    'summary': "This is a module to manage prints and pinters.",
    'depends': ['account', 'sale_management', 'contacts'],
    'data': [
        'security/user_groups.xml',
        'security/ir.model.access.csv',
        'data/printer_types_data.xml',
        'views/menuitems.xml',
        'views/printer_type_views.xml',
        'views/printer_views.xml',
        'views/res_users_views.xml',
        'views/print_views.xml',
        'views/extra_print_views.xml',
        'views/account_move_views.xml',
        ],
}