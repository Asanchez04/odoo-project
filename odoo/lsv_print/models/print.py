"""
This is a python module to manage the prints information
"""
import datetime
import logging
from odoo import api, models, fields, models

_logger = logging.getLogger(__name__)

class Print(models.Model):
    """
    """
    _name = "lsv.print.print"
    _description = "Print model used to manage the prints information."
    _rec_name = "id"
    _order = "create_date"

    DRAFT_STATUS = 'draft'
    IN_PROGRESS_STATUS = 'in_progress'
    SUCCESS_STATUS = 'success'
    ERROR_STATUS = 'error'
    CANCELLED_STATUS = 'cancelled'

    PRINT_STATUS = [
        (DRAFT_STATUS, "Draft"),
        (IN_PROGRESS_STATUS, "In progress"),
        (SUCCESS_STATUS, "Success"),
        (ERROR_STATUS, "Error"),
        (CANCELLED_STATUS, "Cancelled"),
    ]

    state = fields.Selection(PRINT_STATUS, string="State", default=DRAFT_STATUS)
    error_message = fields.Text(string="Error message",
                                required=False)
    printer_id = fields.Many2one('lsv.print.printer',
                                    required=True)
    move_id = fields.Many2one('account.move', string="Move ID.")
    cancelled_on = fields.Datetime(string="Cancelled on", default= None,
                                    required=False)

    def _update_cancelled_info(self):
        """
        update 'cancelled on' and 'error_message' when the status is cancelled
        """
        if self.state == self.CANCELLED_STATUS:
            self.error_message = "Print cancelled by the user manually"
            self.cancelled_on = datetime.datetime.today()
    
    @api.depends('error_message', 'state')
    @api.onchange('error_message')
    def _onchage_error_message(self):
        """
        Override the default cancellation date when the error message is modified.
        """
        if self.error_message and self.state == self.CANCELLED_STATUS:
            self.cancelled_on = datetime.datetime.today()

    @api.onchange('state')
    def _onchange_state(self):
        """
        Add information in the fields 'error_message' and 'cancelled_on' when
        the state is change to "cancelled"
        """
        _logger.error(self.state)
        self.error_message, self.cancelled_on = None, None
        self._update_cancelled_info()
    
    def write(self, vals):
        """ 
        Override the default write function to add the missing fields by example:
        - cancelled_on
        """
        cancelled_on = vals.get('cancelled_on', self.cancelled_on)
        state = vals.get('state', self.state)

        if not cancelled_on and state == self.CANCELLED_STATUS:
            cancelled_on = datetime.datetime.today()
        vals.update({'cancelled_on': cancelled_on})
        _logger.error(vals)
        return super(Print, self).write(vals)
        