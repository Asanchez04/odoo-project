"""
This is a python module to modify the res_users module.
"""
from odoo import models, fields

class ResUser(models.Model):
    """
    Res User inherit model. 
    """
    _inherit = "res.users"
    

    account_move_ids = fields.One2many('account.move', 
                                        'user_id',
                                        string="Account move IDs") 

    printer_id = fields.Many2one('lsv.print.printer', 
                                    string = "Printer", 
                                    required = False,
                                    default = None)