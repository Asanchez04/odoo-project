"""
"""
import logging
from odoo import api, fields, models
from odoo.exceptions import AccessError, UserError, ValidationError

_logger = logging.getLogger(__name__)

class AccountMove(models.Model):
    """
    """
    _name = "account.move"
    _inherit = "account.move"
    
    additional_information = fields.Text(string="Additional information",required=False)
    
    print_ids = fields.One2many('lsv.print.print', 'move_id', string='Prints')
    
    def _create_print_instance(self):
        """
        """
        
        try:
            if not self.write_uid.printer_id:
                raise UserError("The user hasn't printer assigned, please assign a printer first.")
            
            self.env['lsv.print.print'].create([{
                'move_id': self.id,
                'printer_id' : self.write_uid.printer_id.id,
            }])
        except(AccessError, ValidationError, UserError) as error:
            raise UserError(error)
        
        
    def action_invoice_sent(self):
        """
        """
        _logger.error(self.is_move_sent)
        self._create_print_instance()
        output = super(AccountMove, self).action_invoice_sent()
        _logger.error(self.is_move_sent)
        return output                                   

    

    
    #@api.onchange('is_move_sent')
    #def _onchange_is_move_sent(self):
        """
        Apply the following changes when the is_move_sent field is changed to True
        """
       # _logger.error("MOVE ID: {0}, IS MOVE SENT: {1}".format(self.id, self.is_move_sent))