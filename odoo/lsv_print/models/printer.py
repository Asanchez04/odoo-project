"""
This is a python module to manage the lsv.printer.printer module.
"""
from odoo import models, fields

class Printer(models.Model):
    """
    Lsv print.printer module with all attributes and logic
    """
    _name = "lsv.print.printer"
    _description = "Printer model used to manage printer info"
    _rec_name = "name"
    _order = "create_date"

    name = fields.Char(string="Name", 
                        required=True)
    model = fields.Char(string="MOdel", 
                        required=True)

    state = fields.Selection([
        ('available', "Available"),
        ('unavailable', "Unavailable"),
        ('busy', "Busy"),
    ], string="State", required=True)

    print_ids = fields.One2many('lsv.print.print', 
                                inverse_name='printer_id',
                                string="Prints")

    printer_type_id = fields.Many2one('lsv.print.printer.type',
                                        string="Type",
                                        required=True)

    user_ids = fields.One2many('res.users', 
                               inverse_name = 'printer_id',
                              string="Users related")

    #Una impresora tiene 1 tipo y 1 tipo tiene muchas impresoras

    #para que pueda exister un One2many debe existir un Many2one, siempre sera asi. porque se necesita algo que apunte el inverse_name
    #que en este paso seria printer_id que esta ubicado en el modelo print.